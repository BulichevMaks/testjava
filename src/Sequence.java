import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

import static java.lang.System.out;

public class Sequence {

    //1) Проверка на четность:
    //[123,104,75, 81, 1, 8] -> [104,8]

    //2) Проверка суммы цифр элемента на четность:
    //[123,104,75, 81, 1, 8] -> [123,75,8]

    public static int[] filter(int[] array, IntPredicate predicate) {
        return IntStream.of(array).filter(predicate).toArray();
    }

    public static void main(String[] args) {
        int[] array = new int[]{123, 104, 75, 81, 1, 8};
        System.out.println(Arrays.toString(filter(array, value -> value % 2 == 0))); //null <- lambda expression
        System.out.println(Arrays.toString(filter(array, value -> {
            int sum = 0;
            while (value != 0) {
                sum += value % 10;
                value = value / 10;
            }
            value = sum;
            return value % 2 == 0;

        }))); //null <- lambda expression
    }
}
