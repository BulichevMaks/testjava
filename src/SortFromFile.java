import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class SortFromFile {


    public static void main(String[] args) {
        String path1 = "src\\test.txt";
        String path2 = "src\\test2.txt";
        File file1 = new File(path1);
        File file2 = new File(path2);
        ArrayList<Integer> list = new ArrayList<>();
        List<Human> humans = new ArrayList<>();
        humans.add(new Human("Rita", 22));
        humans.add(new Human("Maxim", 2));
        Map<String, Integer> h = new HashMap<>();
        for (Human value : humans) {
            h.put(value.getName(), value.getAge());
        }
        List<Human> smallHumans = humans.stream()
                .filter(human -> human.getAge() > 3)
                .peek(people -> people.setAge(2))
                .toList();
        h = humans.stream().collect(Collectors.toMap(Human::getName, Human::getAge));
        System.out.println(h);
        smallHumans.forEach(System.out::println);

        try (FileInputStream fileInputStream = new FileInputStream(file1)) {
            Scanner scanner = new Scanner(fileInputStream);
            while (scanner.hasNext()) {
                list.add(scanner.nextInt());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collections.sort(list);

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file2))) {
            for (Integer integer : list) {
                bufferedWriter.write(integer.toString());
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
