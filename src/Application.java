

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {

    public static final String НЕ_КОРРЕКТНЫЙ_ВВОД = "Не корректный ввод!";
    public static final String ЧЕЛОВЕК_ДОБАВЛЕН = "Человек добавлен! ";
    public static final String ВВЕДИТЕ_ВОЗРАСТ = "Введите возраст: ";
    public static final String ВВЕДИТЕ_ИМЯ = "Введите имя: ";
    public static final String ПРОДОЛЖИТЬ_ВВОД_Y_OR_N = "Продолжить ввод? \"y or n\" : ";

    public static void main(String[] args) {
        ArrayList<Human> list = new ArrayList<>();
        while (true) {
            System.out.print(ВВЕДИТЕ_ИМЯ);
            String name = new Scanner(System.in).nextLine();
            System.out.print(ВВЕДИТЕ_ВОЗРАСТ);
            int age;
            try {
                age = new Scanner(System.in).nextInt();
                list.add(new Human(name, age));
                System.out.println(ЧЕЛОВЕК_ДОБАВЛЕН);
            } catch (InputMismatchException e) {
                System.out.println(НЕ_КОРРЕКТНЫЙ_ВВОД);
            }

            String str1 = "";
            while (true) {
                System.out.print(ПРОДОЛЖИТЬ_ВВОД_Y_OR_N);
                str1 = new Scanner(System.in).nextLine();
                if (str1.equals("n") || str1.equals("y")) {
                    break;
                }
            }

            if (str1.equals("n")) {
                for (Human h : list) {
                    System.out.println("Имя: \"" + h.getName() + "\", " + "возраст: " + h.getAge());
                }
                break;
            }
        }
    }
}
