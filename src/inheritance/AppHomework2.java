package inheritance;


import java.util.ArrayList;
import java.util.Random;

public class AppHomework2 {
    public static void main(String[] args) {
        Random random = new Random();
        ArrayList<Shape> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            switch (random.nextInt(6)) {
                case 1 -> list.add(new Rectangle(random.nextInt(100), random.nextInt(100)));
                case 2 -> list.add(new Circle(random.nextInt(100)));
                case 3 -> list.add(new Point());
                case 4 -> list.add(new Ellipse(random.nextInt(100), random.nextInt(100)));
                case 5 -> list.add(new Square(random.nextInt(100)));
            }
        }

        for (Shape shape : list) {
            shape.printName();
            System.out.println(shape.area());
        }

        for (Shape shape : list) {
            if (shape instanceof Scalable)
                ((Scalable) shape).scale(2);
        }

        System.out.println("__________________");

        for (Shape shape : list) {
            shape.printName();
            System.out.println(shape.area());
        }
    }
}
