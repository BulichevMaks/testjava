package inheritance;

public class Rectangle extends Shape implements Scalable {

    private double width;
    private double height;

    public Rectangle() {
        super(0, 0);
    }
    public Rectangle(double width, double height) {
        super(0, 0);
        setWidth(width);
        setHeight(height);
    }
    public Rectangle(int x, int y, double width, double height) {
        super(x, y);
        setWidth(width);
        setHeight(height);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double area(){
        return width * height;
    }


    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public void scale(double factor) {
        setHeight(getHeight() * factor);
        setWidth(getWidth() * factor);
    }

    public void printName() {
        System.out.print("Rectangle -> ");
    }
}
