package inheritance;

public class Square extends Rectangle implements Scalable {

    public Square() {
        super(0, 0);
    }

    public Square(double side) {
        super(0, 0);
        setW(side);
    }

    public double getW() {
        return getWidth();
    }

    public void setW(double side) {
        setWidth(side);
        setHeight(side);
    }

    @Override
    public void setHeight(double side) {
        super.setHeight(side);
        super.setWidth(side);
    }

    @Override
    public void setWidth(double side) {
        super.setHeight(side);
        super.setWidth(side);
    }

    @Override
    public double area() {
        return getWidth() * getWidth();
    }

    @Override
    public String toString() {
        return "Square{" +
                "width=" + getWidth() +
                ", height=" + getWidth() +
                '}';
    }


    public void printName() {
        System.out.print("Square -> ");
    }

    @Override
    public void scale(double factor) {
        setW(getW() * factor);
    }
}