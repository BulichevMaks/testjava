package inheritance;

public class Ellipse extends Shape implements Scalable {

    private double r1;
    private double r2;

    public Ellipse(double r1, double r2) {
        super(0, 0);
        setR1(r1);
        setR2(r2);
    }

    public double getR1() {
        return r1;
    }

    public void setR1(double r1) {
        this.r1 = r1;
    }

    public double getR2() {
        return r2;
    }

    public void setR2(double r2) {
        this.r2 = r2;
    }

    @Override
    public double area(){
        return Math.PI * r1 * r2;
    }


    @Override
    public String toString() {
        return "Ellipse{" +
                "r1=" + r1 +
                ", r2=" + r2 +
                '}';
    }


    public void printName() {
        System.out.print("Ellipse -> ");
    }

    @Override
    public void scale(double factor) {
        setR1(getR1() * factor);
        setR2(getR2() * factor);
    }
}
