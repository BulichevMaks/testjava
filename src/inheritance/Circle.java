package inheritance;

public class Circle extends Ellipse implements Scalable {
    public Circle(double r) {
        super(0, 0);
        setR(r);
    }
    public Circle(int x, int y, double r) {
        super(x, y);
        setR(r);
    }

    public double getR() {
        return getR2();
    }

    public void setR(double r) {
        super.setR1(r);
        super.setR2(r);
    }

    @Override
    public void setR1(double r) {
        setR(r);
    }

    @Override
    public void setR2(double r) {
        setR(r);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "r=" + getR() +
                '}';
    }

    @Override
    public void scale(double factor) {
        setR(getR() * factor);
    }

    @Override
    public void printName() {
        System.out.print("Circle -> ");
    }
}
