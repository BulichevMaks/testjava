package inheritance;

public class Point extends Shape {

    public Point() {
        super(0, 0);
    }

    public Point(int x, int y) {
        super(x, y);
    }

    @Override
    public double area() {
        return 0;
    }

    public String toString() {
        return "Point{" +
                "X=" + getX() + "Y=" + getY() +
                '}';
    }

    public void printName() {
        System.out.print("Point -> ");
    }

}
